from .context import registration, Store
from uuid import uuid4

def test_register():
    client_addresses = [str(uuid4()), str(uuid4())]
    store = Store()
    deposit_address = registration.register(store, client_addresses)
    assert deposit_address != None
    assert deposit_address in store.list_deposit_addresses()
    assert client_addresses == store.get_client_addresses(deposit_address)
