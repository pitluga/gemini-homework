from .context import StubAPI, Store, Mixer
from decimal import Decimal
from random import Random

def test_mixer_sweeps_funds_to_house_account():
    api = StubAPI()
    store = Store()
    store.store_deposit_address('deposit_addr', ['user_1', 'user_2'])
    mixer = Mixer('house_addr', api, store, Random())

    mixer.mix('deposit_addr', {'amount': '10'})

    assert api._transfers == [('deposit_addr', 'house_addr', '10')]

def test_mixer_schedules_settlements_in_the_future():
    store = Store()
    store.store_deposit_address('deposit_addr', ['user_1', 'user_2'])
    mixer = Mixer('house_addr', StubAPI(), store, Random())

    mixer.mix('deposit_addr', {'amount': '10'})

    total = Decimal(0)
    for settlement in store.list_pending_settlement_transfers():
        total += settlement.amount

    assert total == Decimal(10)
