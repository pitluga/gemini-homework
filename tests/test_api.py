from .context import api
from uuid import uuid4

def test_addresses():
    result = api.transactions('test')
    assert result.success == True
    assert result.value['balance'] == '50'
    assert len(result.value['transactions']) == 1
    assert result.value['transactions'][0]['toAddress'] == 'test'

def test_create_creates_a_new_address_with_50_job_coins():
    address = str(uuid4())
    result = api.create(address)
    assert result.success == True

    result = api.transactions(address)
    assert result.success == True
    assert result.value['balance'] == '50'

def test_transfer_moves_money_from_one_address_to_another():
    sender = str(uuid4())
    receiver = str(uuid4())

    assert api.create(sender).success == True
    assert api.create(receiver).success == True

    result = api.transfer(sender, receiver, '20')
    assert result.success == True

    result = api.transactions(sender)
    assert result.success == True
    assert result.value['balance'] == '30'

    result = api.transactions(receiver)
    assert result.success == True
    assert result.value['balance'] == '70'
