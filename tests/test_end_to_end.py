from .context import registration, Scanner, Settler, Store, api, Mixer
from uuid import uuid4
from secrets import SystemRandom
from decimal import Decimal

def sum_balances(*addresses):
    balance = Decimal(0)
    for address in addresses:
        result = api.transactions(address)
        assert result.success
        balance += Decimal(result.value['balance'])

    return balance

def test_happy_path_end_to_end():
    house = str(uuid4())
    feeder1, feeder2 = str(uuid4()), str(uuid4())
    fred1, fred2, fred3 = str(uuid4()), str(uuid4()), str(uuid4())
    george1, george2 = str(uuid4()), str(uuid4())

    assert api.create(feeder1).success
    assert api.create(feeder2).success

    store = Store()
    mixer = Mixer(house, api, store, SystemRandom())
    scanner = Scanner(store, api, mixer)
    settler = Settler(api, store)

    fred_deposit = registration.register(store, [fred1, fred2, fred3])
    george_deposit = registration.register(store, [george1, george2])

    scanner.scan()

    assert len(store.list_pending_settlement_transfers()) == 0

    assert api.transfer(feeder1, fred_deposit, '12.20').success
    assert api.transfer(feeder1, george_deposit, '29.00').success
    assert api.transfer(feeder2, george_deposit, '44.00').success

    scanner.scan()

    assert len(store.list_pending_settlement_transfers()) > 0

    settler.settle()

    assert sum_balances(fred1, fred2, fred3) == Decimal('12.20')
    assert sum_balances(george1, george2) == Decimal('73.00')
    assert sum_balances(house) == Decimal('0')
