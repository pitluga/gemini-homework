from .context import StubAPI, Settler, SettlementTransfer, Store
from datetime import datetime
from decimal import Decimal

def test_settler_settles_due_transfers():
    api = StubAPI()
    store = Store()
    store.insert_settlement_transfer(SettlementTransfer(datetime.now(), 'house', 'jane', Decimal('10')))

    settler = Settler(api, store)

    settler.settle()

    assert api._transfers == [('house', 'jane', '10')]
