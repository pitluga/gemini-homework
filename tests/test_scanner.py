from .context import StubAPI, Store, StubMixer, Scanner
from uuid import uuid4

def test_scanner_mixes_new_transactions():
    deposit_address = str(uuid4())
    store = Store()
    store.store_deposit_address(deposit_address, [str(uuid4())])
    api = StubAPI()
    api._transactions = {
        deposit_address: {
            'balance': '30.1',
            'transactions': [
                {'timestamp': '2014-04-23T18:25:43.511Z',
                'toAddress': deposit_address,
                'amount': '30.1'}
            ]
        }
    }
    mixer = StubMixer()
    scanner = Scanner(store, api, mixer)
    scanner.scan()

    assert mixer.mixes == [(deposit_address, '2014-04-23T18:25:43.511Z')]

def test_scanner_does_not_mix_already_mixed_transactions():
    pass

def test_scanner_handles_api_errors():
    pass
