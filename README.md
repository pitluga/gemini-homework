# Jobcoin Mixer

This is my jobcoin mixer implementation. It is what I would call spike or proof-of-concept quality code. There is nothing to run. To view the system being exercied as a whole, check out `tests/test_end_to_end.py`.

# To verify

requirements: Python 3.7

```bash
$ python3 -m venv .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt
$ pytest
```

# Business Decisions

I tried to focus most on the different components and how I would build this. The only obvious user-interactive piece is the registration. This is just a single function. This would probably be exposed via a REST API or something.

There are two jobs that would run in the background.

* Scanner - this searches for new transactions in the deposit addresses and for each transaction, it sweeps the funds to the house account and schedule future transfers from the house account to the client accounts.

* Settler - this will perform transfers to client accounts when they are due. Right now it just transfers everything when it runs.

Mixing is very naive and really I just got it to work when I stopped. It attempts to break the tranfer up into random chunks and schedule them at random times in the next 6 hours. I don't think it works very well, but it doesn't lose money. I'd improve it by doing the following:

* mix multiple transactions together
* do a better job of chunking things


# Software Design

I'm a big fan of TDD and dependency injection. I think this codebase is a pretty good representation of how I like to design code in an Object-Oriented langauge. Small, single purpose objects that are stateless and have all their dependencies passed via a constructor. My tests tend to swap out slow dependencies with either hand rolled stubs or other substitutes.
