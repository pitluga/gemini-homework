class Settler:
    def __init__(self, api, store):
        self.api = api
        self.store = store

    def settle(self):
        for settlement in self.store.list_pending_settlement_transfers():
            self.api.transfer(settlement.sender, settlement.receiver, str(settlement.amount))
