from .api import Result

class StubAPI:
    def __init__(self):
        self._transactions = {}
        self._transfers = []

    def transactions(self, address):
        return Result(success=True, value=self._transactions.get(address, {}))

    def transfer(self, sender, receiver, amount):
        self._transfers.append((sender, receiver, amount))
        return Result(success=True)


