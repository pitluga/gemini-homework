class StubMixer:
    def __init__(self):
        self.mixes = []

    def mix(self, deposit_address, transaction):
        self.mixes.append((deposit_address, transaction['timestamp']))

