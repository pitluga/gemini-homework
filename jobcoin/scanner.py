class Scanner:
    def __init__(self, store, api, mixer):
        self.store = store
        self.api = api
        self.mixer = mixer

    def scan(self):
        for deposit_address in self.store.list_deposit_addresses():
            result = self.api.transactions(deposit_address)
            if result.success:
                for transaction in result.value['transactions']:
                    if self.store.attempt_save_sweep(deposit_address, transaction['timestamp']):
                        self.mixer.mix(deposit_address, transaction)
