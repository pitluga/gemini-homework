from uuid import uuid4

def register(store, client_addresses):
    deposit_address = str(uuid4())
    store.store_deposit_address(deposit_address, client_addresses)
    return deposit_address
