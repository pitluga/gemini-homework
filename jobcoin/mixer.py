from decimal import Decimal
from datetime import datetime, timedelta
from dataclasses import dataclass

MAX_SPLITS = 10
MAX_LAG_MINUTES = 360 # 6 hours

@dataclass
class SettlementTransfer:
    at: datetime
    sender: str
    receiver: str
    amount: Decimal

class Mixer:
    def __init__(self, house_address, api, store, random):
        self.house_address = house_address
        self.api = api
        self.store = store
        self.random = random

    def mix(self, deposit_address, transaction):
        result = self.api.transfer(deposit_address, self.house_address, transaction['amount'])
        if result.success:
            num_splits = self.random.randint(1, MAX_SPLITS)
            client_addresses = self.store.get_client_addresses(deposit_address)

            remaining_amount = Decimal(transaction['amount'])
            # if we wanted to extract fees, that would go right here

            for split in range(0, num_splits):
                settle_xfer = self._create_transfer(client_addresses, remaining_amount, split+1 == num_splits)
                remaining_amount = remaining_amount - settle_xfer.amount
                self.store.insert_settlement_transfer(settle_xfer)

    def _create_transfer(self, client_addresses, remaining_amount, is_last):
        lag = timedelta(minutes=self.random.randint(1, MAX_LAG_MINUTES))
        address = self.random.choice(client_addresses)
        if is_last:
            amount = remaining_amount
        else:
            amount = (remaining_amount * Decimal(self.random.random())).quantize(Decimal('0.01'))

        return SettlementTransfer(datetime.now() + lag, self.house_address, address, amount)
