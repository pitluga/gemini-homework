from collections import defaultdict

class Store:
    def __init__(self):
        self.deposit_addresses = {}
        self.sweeped = defaultdict(set)
        self.settlement_transfers = []

    def store_deposit_address(self, deposit_address, client_addresses):
        self.deposit_addresses[deposit_address] = client_addresses

    def get_client_addresses(self, deposit_address):
        return self.deposit_addresses[deposit_address]

    def list_deposit_addresses(self):
        return self.deposit_addresses.keys()

    def attempt_save_sweep(self, deposit_address, timestamp):
        if timestamp in self.sweeped[deposit_address]:
            return False
        else:
            self.sweeped[deposit_address].add(timestamp)
            return True

    def insert_settlement_transfer(self, settlement_transfer):
        self.settlement_transfers.append(settlement_transfer)

    def list_pending_settlement_transfers(self):
        return self.settlement_transfers
