import requests
from dataclasses import dataclass

API_ENDPOINT = 'https://jobcoin.gemini.com/unaired'

@dataclass
class Result:
    success: bool
    value: any = None

def transactions(address):
    response = requests.get(f"{API_ENDPOINT}/api/addresses/{address}")
    if response.status_code == 200:
        return Result(success=True, value=response.json())
    else:
        return Result(success=False)

def create(address):
    response = requests.post(f"{API_ENDPOINT}/create", data={'address': address})
    if response.status_code == 200:
        return Result(success=True)
    else:
        return Result(success=False)


def transfer(sender, receiver, amount):
    response = requests.post(f"{API_ENDPOINT}/api/transactions", json={'fromAddress': sender, 'toAddress': receiver, 'amount': amount})
    if response.status_code == 200 and response.json()['status'] == 'OK':
        return Result(success=True)
    else:
        return Result(success=False)
